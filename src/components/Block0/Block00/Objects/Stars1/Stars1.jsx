import React from 'react';
import {NavLink} from "react-router-dom"
// import s from './Stars1.module.css';

const Stars1Items = (props) => {
  let path = "/objects/stars/" + props.id;
  return <div>
    <NavLink to={path}>{props.name}</NavLink>
  </div>
}

const Stars1 = (props) => {
  let Stars1Template = props.Stars1Base
  .map( Template => <Stars1Items name={Template.name} id={Template.id}/> );
  return <div> {Stars1Template} </div>
}

export default Stars1;
