import React from 'react';
import w from './Wall1.module.css';
import {Route} from "react-router-dom"

const Wall1Items = (props) => {
  let path1 = "/objects/stars/" + props.id;
  return <div>
    <Route path={path1}>{props.name}</Route>
  </div>
}

const Wall1 = (props) => {
  let Wall1StarsBase = [
    {id: 0, name: 'Жёлтый карлик'},
    {id: 1, name: 'Красный гигант'},
    {id: 2, name: 'Белый карлик'}
  ];
  return (
    <div className={w.Wall1}>
      <Wall1Items name={Wall1StarsBase[0].name} id={Wall1StarsBase[0].id}/>
      <Wall1Items name={Wall1StarsBase[1].name} id={Wall1StarsBase[1].id}/>
      <Wall1Items name={Wall1StarsBase[2].name} id={Wall1StarsBase[2].id}/>
    </div>
  );
}

export default Wall1;
