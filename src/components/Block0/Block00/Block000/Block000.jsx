import React from 'react';
import b0 from './Block000.module.css';
import Block1 from './Block1/Block1';
import Block2 from './Block2/Block2';
import Wall1 from './Wall1/Wall1';

const Block000 = (props) => {
  return (
    <div className={b0.Block000}>
      <Block1/>
      <Block2 state={props.state}/>
      <Wall1/>
    </div>
  )
}

export default Block000;
