import React from 'react';
import b from './Block2.module.css';
import Stars1 from '../../Objects/Stars1/Stars1';
import Planets1 from '../../Objects/Planets1/Planets1';
import NaturalBody1 from '../../Objects/NaturalBody1/NaturalBody1';
import {Route} from "react-router-dom"

const Block2 = (props) => {
  return (
    <div className={b.Block2}>
      <Route path='/objects/stars' render={ () => <Stars1 Stars1Base={props.state.Stars1Base}/>}/>
      <Route path='/objects/planets' render={ () => <Planets1 Planets1Base={props.state.Planets1Base}/>}/>
      <Route path='/objects/naturalBody' render={ () => <NaturalBody1 NaturalBody1Base={props.state.NaturalBody1Base}/>}/>
    </div>
  )
}

export default Block2;
