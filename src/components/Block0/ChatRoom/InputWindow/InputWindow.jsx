import React from 'react';
import i from './InputWindow.module.css';

const InputWindow = () => {
  return (
    <div className={i.InputWindow}>
      <input type="text" size="40" value="Дима няшка"/>
      <button>Save</button>
    </div>
  )
}

export default InputWindow;
