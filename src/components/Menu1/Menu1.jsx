import React from 'react';
import m from './Menu1.module.css';
import {NavLink} from "react-router-dom"

const Menu1 = () => {
  return <header className={m.Menu1}>
    <div className={`${m.div1} ${m.active}`}><NavLink to="/home">Главная</NavLink></div>
    <div className={m.div1}><NavLink to="/objects">Объекты</NavLink></div>
    <div className={m.div1}><NavLink to="/hobby">Хобби</NavLink></div>
    <div className={m.div1}><NavLink to="/chatRoom">Чат</NavLink></div>
  </header>
}

export default Menu1;
